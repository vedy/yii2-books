<?php

  namespace app\models;
  use yii\db\ActiveRecord;
  use \yii\db\Expression;
  use yii\behaviors\TimestampBehavior;

  class Book extends ActiveRecord {

    public static function model($className = __CLASS__) {
      return parent::model($className);
    }

    public static function tableName() {
      return 'books';
    }

    public static function primaryKey() {
      return array('id');
    }

    public function rules()
    {
        return [
             [['name', 'date', 'author_id'], 'required'],
            ['name', 'string', 'message' => 'Пожалуйста, укажите название книги'],
            ['date', 'string', 'message' => 'Пожалуйста, укажите дату издания книги'],
            ['author_id', 'integer', 'message' => 'Пожалуйста, укажите автора книги'],
            ['preview', 'safe'],
            ['preview', 'file', 'extensions' => 'jpeg, jpg, gif, png'],
        ];
    }
    public function getAuthor()
    {
        return $this->hasOne(Author::className(), ['id' => 'author_id']);
    }

    public function attributeLabels() {
      return array(
        'id' => 'ID',
        'name' => 'Наименование',
        'date_create' => 'Дата добавления в архив',
        'date_update' => 'Дата обновления',
        'preview' => 'Обложка',
        'date' => 'Дата издания',
        'author_id' => 'Автор');
    }


    public function behaviors()
    {
        return [
            [
                 'class' => '\yiidreamteam\upload\ImageUploadBehavior',
                 'attribute' => 'preview',
                 'thumbs' => [
                     'thumb' => ['width' => 75, 'height' => 100],
                 ],
                 'filePath' => '@webroot/uploads/[[model]]-[[pk]].[[extension]]',
                 'fileUrl' => '/rgkgroup/uploads/[[model]]-[[pk]].[[extension]]',
                 'thumbPath' => '@webroot/uploads/[[model]]-[[pk]]-tn.[[extension]]',
                 'thumbUrl' => '/rgkgroup/uploads/[[model]]-[[pk]]-tn.[[extension]]',
            ],
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'date_create',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'date_update',
                ],
                'value' => function() { return date('U'); },
            ],
        ];
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        if ($this->getIsNewRecord()) {
            return $this->insert($runValidation, $attributeNames);
        } else {
            return $this->update($runValidation, $attributeNames) !== false;
        }
    }


    public function beforeSave($insert)
    {
        $this->date = strtotime($this->date);
        return parent::beforeSave($insert);
    }

  }


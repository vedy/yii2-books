<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

?>
<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button><br />
<div id="success"> </div> <!-- For success message -->

<div class="gb-user-form">

    <?php $form = ActiveForm::begin([
      'action' => 'site/submitadd',
      'method' => 'post',
      'options' => [
        'class' => 'add-form',
        'enableClientValidation' => false,
        'enctype' => 'multipart/form-data',
      ]]); ?>

      <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
      <?= $form->field($model, 'author_id')->dropDownList($authors,['prompt'=>'Выберите автора']) ?>
      <?= $form->field($model, 'date')->textInput(['maxlength' => 255,'type'=> 'date']) ?>
      <?= $form->field($model, 'preview')->fileInput(['maxlength' => 255]) ?>

    <div class="form-group text-right">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> 

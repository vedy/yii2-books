
  function buildSearchData(){
    var obj = {
      "cmd" : "refresh",
      "SearchForm[author_id]": $("#searchform-author_id").val(),
      "SearchForm[name]": $("#searchform-name").val(),
      "SearchForm[year_start]": $("#searchform-year_start").val(),
      "SearchForm[year_end]": $("#searchform-year_end").val(),
    };
    return obj;
  }

$(document).ready(function () {
  if($.fn.dataTable!=undefined) $.fn.dataTable.ext.errMode = 'none';



  $(document).on("click", '#searchclear', function(){
      $(this).prev('input').val('');
      $("#search-form").submit();
  });


  $(document).on("click", '.add',function (event) {
    event.preventDefault();
    var url = this.href;
    var clickedbtn = $(this);

    var modalContainer = $('#my-modal');
    
    modalContainer.modal({show: true});
    $.ajax({
      url:     url,
      type:    "GET",
      data:    {},
      success: function (data) {
        $('.modal-body').html(data);
        modalContainer.modal({show: true});
      }
    });
  });

  $(document).on("submit", '.add-form', function (e) {
    e.preventDefault();
    var formData = new FormData($(this)[0]);

    $.ajax({
      url:     $(this).attr("action"),
      type:    "POST",
      data:    formData,
      datatype: 'json',
      processData: false,
      async: false,
      cache: false,
      contentType: false,
      success: function (result) {
        if ('update' in result) {
          $("#search-form").submit();
          setTimeout(function () {  $("#my-modal").modal('hide'); }, 500);
        }
        else $('.modal-body').html(result);
      }
    });
  });

  $(document).on("submit", '.edit-form', function (e) {
    e.preventDefault();
    var formData = new FormData($(this)[0]);
    var bid = $(this).attr("bid");
    $.ajax({
      url:     $(this).attr("action"),
      type:    "POST",
      data:    formData,
      datatype: 'json',
      processData: false,
      async: false,
      cache: false,
      contentType: false,
      success: function (result) {
        if(window.opener && !window.opener.closed) {
          window.opener.$("#search-form").submit();
          window.close();
        }
        else if (!window.opener && 'update' in result) {
          $("#search-form").submit();
          setTimeout(function () {  $("#my-modal").modal('hide'); }, 500);
        }
        else window.location('/rgkgroup/')
      }
    });
  });


  $(document).on("click", ".thickbox" , function (e) {
    e.preventDefault();
    var modalContainer = $('#my-modal');
    
    modalContainer.modal({show: true});
    $('.modal-body').html('<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button><div class="model_view"><img src="'+this.href+'" /></div>');
    modalContainer.modal({show: true});

    return false;
  });

  $(document).on("click", ".icon-view" , function (e) {
    e.preventDefault();
    var modalContainer = $('#my-modal');
    
    modalContainer.modal({show: true});
    var bid = $(this).attr("bid");
    $.ajax({
      url:     $(this).attr("href"),
      type:    "GET",
      success: function (result) {
          $('.modal-body').html(result);
          modalContainer.modal({show: true});
      }
    });

    return false;
  });

  $(document).on("click", ".icon-edit" , function (e) {
    e.preventDefault();
    window.open($(this).attr("href"),'ModaForm','width=600, height=520');
    return false;
  });


  $(document).on("click", ".icon-trash" , function (e) {
    e.preventDefault();
    if(!confirm('Точно-точно удалить?')) return;
    var bid = $(this).attr("bid");
    $.ajax({
      url:     $(this).attr("href"),
      type:    "GET",
      success: function (result) {
        if (result == true) {
          $("#search-form").submit();
        }
      }
    });
    return false;
  });


  $(document).on("submit", '#search-form', function (e) {
    e.preventDefault();
    $('#data-table').DataTable().ajax.reload( null, false );

  });

  $("#data-table").DataTable({
    ajax: {
      "url":  'site/search',
      "type": 'POST',
      "data": buildSearchData,
      //processData: false
    },
    searching:  false,
    pagingType: "full_numbers",
    info:       false,
    stateSave:  true,
    lengthMenu: [[2, 5, 10, 25, 50, -1], [2, 5, 10, 25, 50, "все"]],
    language: {
      "processing": "Подождите...",
      "search": "Поиск:",
      "lengthMenu": "Показывать по _MENU_ шт. на страницу",
      "zeroRecords": "Нет записей",
      "info": "Записи с _START_ до _END_ из _TOTAL_ ",
      "infoEmpty": "Нет записей",
      "infoFiltered": "(отфильтровано из _MAX_)",
      "infoPostFix": "",
      "loadingRecords": "Загрузка записей...",
      "emptyTable": "В таблице отсутствуют данные",
      "paginate": {
          "first": "Первая",
          "previous": "Предыдущая",
          "next": "Следующая",
          "last": "Последняя"
        },
        "aria": {
          "sortAscending": ": активировать для сортировки столбца по возрастанию",
          "sortDescending": ": активировать для сортировки столбца по убыванию"
        }
    },
    columns: [
      { data: "id" },
      { data: "name" },
      { data: {_: "preview", sort: false} },
      { data: "author" },
      { data: {
        _:    "date_release.display",
        sort: "date_release.timestamp"
      } },
      { data: {
        _:    "date_create.display",
        sort: "date_create.timestamp"
      } },
      { data: {_: "edit", sort: false} },
      { data: {_: "update", sort: false} },
      { data: {_: "delete", sort: false} }
    ],
    columnDefs: [{
      "targets": [ 2, 6, 7, 8 ],
      "visible": true,
      "sortable": false
    }],
		"dom": '<"top"lp>rt<"bottom"ip>'
  });



});

<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;

?>
<?php if($ajax) { ?>
<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button><br />
<?php }
else {
  $this->registerJsFile('/rgkgroup/scripts/index.js',['depends'=>'app\assets\AppAsset']);
  ?>
  <h3>Редактирование свойств книги</h3>
 <?php   } ?>
<div id="success"> </div> <!-- For success message -->

<div class="edit-user-form">

    <?php $form = ActiveForm::begin([
      'action' => '@web/site/submitupdate/'.$model->id,
      'method' => 'post',
      'options' => [
        'class' => 'edit-form',
        'enableClientValidation' => false,
        'enctype' => 'multipart/form-data',
        'bid' => $model->id
      ]]); ?>

      <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
      <?= $form->field($model, 'author_id')->dropDownList($authors) ?>
      <?= $form->field($model, 'date')->textInput(['maxlength' => 255, 'type'=> 'date', 'value'=>strftime('%Y-%m-%d',$model->date)]) ?>
      <?= $form->field($model, 'preview')->fileInput(['maxlength' => 255]) ?>

    <div class="form-group text-right">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> 

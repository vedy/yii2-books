<?php
  namespace app\models;

  use yii\db\ActiveRecord;

  class Author extends ActiveRecord
  {
    public static function model($className=__CLASS__)
    {
      return parent::model($className);
    }

    public static function tableName()
    {
      return 'authors';
    }
    public static function primaryKey()
    {
      return array('id');
    }
    public function getNamesArray()
    {
      $authors = array();
      $all     = parent::find()->asArray()->all();
      foreach ($all as $author) {
        $authors[$author['id']] = $author['firstname'].' '.$author['lastname'];
      }
      return  $authors;
    }
    public function attributeLabels()
    {
      return array(
        'id' => 'ID',
        'firstname' => 'Имя',
        'lastname' => 'Фамилия',
      );
    }
  }

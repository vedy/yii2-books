<?php
  use yii\helpers\Html;
  use yii\bootstrap\ActiveForm;
  use yii\helpers\ArrayHelper;

  $this->registerCssFile("/rgkgroup/scripts/datatables/css/jquery.dataTables.min.css");
  $this->registerJsFile('/rgkgroup/scripts/datatables/js/jquery.dataTables.js',['depends'=>'app\assets\AppAsset']);
  $this->registerJsFile('/rgkgroup/scripts/index.js',['depends'=>'app\assets\AppAsset']);

$this->title = 'Архив книг:: My Yii Application';

?>
<div class="site-index">


    <h1>Архив книг</h1>


    <div class="body-content">

        <div class="row">
          <h3>Поиск:</h3>

          <?php $form = ActiveForm::begin([
            'action' => 'site/search',
            'id' => 'search-form',
            'options' => [
              'class' => 'search-form',
              'enableClientValidation' => false,
              'enctype' => 'multipart/form-data',
            ]]);
          ?>

          <div class="search-user-form col-lg-4">

            <?php echo $form->field($search, 'author_id')->dropDownList($authors,['class' => 'form-control','prompt'=>'автор'])->label(false); ?>
          </div>

          <div class="search-user-form col-lg-3">

            <?php echo $form->field($search, 'name',['template' => '{label} {input}<span id="searchclear" class="glyphicon glyphicon-remove-circle"></span>{error}{hint}'])->textInput(['maxlength' => 255, 'class' => 'form-control', 'placeholder'=>'название книги'])->label(false); ?>

          </div>
          <div class="search-user-form col-lg-1">

          </div>
        </div>
        <div class="row">
          <div class="search-user-form col-lg-2">
						<label>Дата издания от:</label>
          </div>
          <div class="search-user-form col-lg-2">
                <?php echo $form->field($search, 'year_start')->textInput(['type'=>'date','maxlength' => 10,'class' => 'form-control', 'placeholder'=>'Дата издания от'])->label(false); ?>
          </div>
          <div class="search-user-form col-lg-1">
						<label>до:</label>
          </div>
          <div class="search-user-form col-lg-2">
                <?php echo $form->field($search, 'year_end')->textInput(['type'=>'date','maxlength' => 10,'class' => 'form-control', 'placeholder'=>'Дата издания до'])->label(false); ?>
          </div>

          <div class="search-user-form col-lg-1">



						<div class="form-group text-right">
								<?= Html::submitButton('Искать', ['class' => 'btn btn-success']) ?>
						</div>


					</div>
        </div>
          <?php ActiveForm::end(); ?>

    </div>
  <hr />
  <div class="row">
		<div class="col-lg-11"></div>
		<div class="col-lg-1">
			<?php if(!$guest) { ?>
			<p><?php echo Html::a('Добавить книгу', array('site/add'), array('class'=>'btn btn-success add')); ?></p>
			<?php } ?>
		</div>
    </div>
		<div class="row">
			<div class="col-lg-13">

				<table class="display table table-striped table-hover" id="data-table">
					<thead>
						<tr>
								<th>ID</th>
								<th>Название</th>
								<th data-sorter="false">Превью</th>
								<th>Автор</th>
								<th>Дата выхода книги</th>
								<th>Дата добавления</th>
							<?php if(!$guest) { ?>
								<th colspan="3" data-sorter="false">Действия</th>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
		</div>
  </div>


  <div class="modal fade" id="my-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog">
       <div class="modal-content">
         <div class="modal-body">
         </div>
       </div>
     </div>
  </div>

</div>

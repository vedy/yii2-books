<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SearchForm;
use app\models\Book;
use app\models\Author;


use yii\helpers\Html;
use yii\i18n\Formatter;
Yii::$app->formatter->locale = 'ru-RU';

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','add','update','submitadd','submitedit','show'],
                'rules' => [
                    [
                        'actions' => ['logout','add','update','submitadd','submitedit','show'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

  public function actionIndex()
  {

    $modelBook   = new Book;
    $modelAuthor = new Author();
    $modelSearch = new SearchForm();

    $authors = $modelAuthor->getNamesArray();
    $data    = $modelBook->find()->with('author')->all(); //


    return $this->render('index', array(
        'data' => $data,
        'model'=>$modelBook,
        'search'=>$modelSearch,
        'authors' => $authors,
        'guest'=>\Yii::$app->user->isGuest )
    );
  }

  public function actionSearch()
  {
    $books = Book::find();

    $search_name       = Yii::$app->request->post('SearchForm')['name'];
    $search_yearstart = strtotime(Yii::$app->request->post('SearchForm')['year_start']);
    $search_yearend = strtotime(Yii::$app->request->post('SearchForm')['year_end']);
    $search_author = Yii::$app->request->post('SearchForm')['author_id'];

    $mult = false;
    if (!empty($search_name)) {
      $books->andWhere(['like', 'name', $search_name]);
      $mult = true;
    }
    if (!empty($search_yearstart)) {
      $books->andWhere(['>=', 'date', $search_yearstart]);
      $mult = true;
    }
    if (!empty($search_yearend)) {
      $books->andWhere(['<=', 'date', $search_yearend]);
      $mult = true;
    }
    if (!empty($search_author)) {
      $books->andWhere(['author_id'=> $search_author]);
      $mult = true;
    }
    $selection = $books->all();
    $dataArray = array();
    foreach ($selection as $book) {
      $bookData = [
        'id'=>$book->id,
        'name'=>$book->name,
        'preview'=>'<a href="'.$book->getImageFileUrl('preview','/rgkgroup/uploads/empty.jpg').'" class="thickbox">
                      <img src="'.$book->getThumbFileUrl('preview', 'thumb', '/rgkgroup/uploads/thumb_empty.jpg').'" />
                    </a>',
        'author'=>$book->author->firstname.' '.$book->author->lastname,
        'date_release'=>[
          'display'=>Yii::$app->formatter->asDate($book->date),
          'timestamp'=>$book->date
        ],
        'date_create'=>[
          'display'=>($book->date_create <  time()-3600*24)? Yii::$app->formatter->asDateTime($book->date_create) : str_replace('день назад','вчера',Yii::$app->formatter->asRelativeTime($book->date_create)),
          'timestamp'=>$book->date_create
        ],
      ];
      if (!\Yii::$app->user->isGuest) {
        $bookData['edit'] = Html::a(' ред.', array('site/update', 'id'=>$book->id), array('class'=>'glyphicon glyphicon-edit icon-edit','bid'=>$book->id));
        $bookData['update'] = Html::a(' просм.', array('site/show', 'id'=>$book->id), array('class'=>'glyphicon glyphicon-eye-open icon-view','bid'=>$book->id));
        $bookData['delete'] = Html::a(' удал.', array('site/delete', 'id'=>$book->id), array('class'=>'glyphicon glyphicon-trash icon-trash','bid'=>$book->id));
      }
      $dataArray[] = $bookData;
    }
    Yii::$app->response->format = Response::FORMAT_JSON;
    return ['data'=>$dataArray];
  }

  public function actionShow($id)
  {
    $modelBook = Book::find()->where(['id' => $id])->one();
    return $this->renderPartial('view', ['book' =>  $modelBook]);
  }

  public function actionDelete($id)
  {
    Yii::$app->response->format = Response::FORMAT_JSON;
    $modelBook = Book::find()->where(['id' => $id])->one();
    if($modelBook && $modelBook->delete()) return true;
    return false;
  }


    public function actionAdd()
    {
      $modelBook = new Book();
      $modelAuthor = new Author();
      $authors = $modelAuthor->getNamesArray();

      return $this->renderPartial('addfrom', ['model' =>  $modelBook,'authors' => $authors]);
    }

    public function actionSubmitadd()
    {
      Yii::$app->response->format = Response::FORMAT_JSON;
      $model = new Book();
      $model->load(Yii::$app->request->post());

      $modelAuthor = new Author();
      $authors = $modelAuthor->getNamesArray();


      if($model->load(Yii::$app->request->post()) && $model->save()) {
          return array('update'=>true);
      }
      else
      {
          return array($this->renderPartial('addfrom', ['model' => $model,'authors' => $authors]));
      }
    }

    public function actionUpdate($id)
    {
      $request = Yii::$app->request;
      $modelBook = Book::find()->where(['id' => $id])->one();
      $modelAuthor = new Author();
      $authors = $modelAuthor->getNamesArray();

      if ($request->isAjax)
        return $this->renderPartial('editform', ['model' =>  $modelBook,'authors' => $authors,'ajax'=>true]);
      else
        return $this->render('editform', ['model' =>  $modelBook,'authors' => $authors,'ajax'=>false]);
    }

    public function actionSubmitupdate($id)
    {
      Yii::$app->response->format = Response::FORMAT_JSON;
      $model = Book::find()->where(['id' => $id])->one();
      $model->load(Yii::$app->request->post());

      $modelAuthor = new Author();
      $authors = $modelAuthor->getNamesArray();

      if($model->load(Yii::$app->request->post()) && $model->save()) {
        return array('update'=>true);
      }
      else
      {
        return array($this->renderPartial('editform', ['model' => $model,'authors' => $authors]));
      }
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}

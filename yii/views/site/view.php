<?php

  use yii\helpers\Html;
  use yii\i18n\Formatter;
  Yii::$app->formatter->locale = 'ru-RU';


?>
  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>

  <div id="model-<?php echo $book->id; ?>" class="model_view">
    <h2><?php echo $book->name; ?></h2>
    <h3><?php echo $book->author->firstname.' '.$book->author->lastname; ?></h3>
    <img src="<?php echo $book->getImageFileUrl('preview'); ?>"  />
    <p>Год Издания: <?php echo Yii::$app->formatter->asDate($book->date); ?></p>
    <p>Дата добавления в базу: <?php echo ($book->date_create <  time()-3600*24)? Yii::$app->formatter->asDateTime($book->date_create) : str_replace('день назад','вчера',Yii::$app->formatter->asRelativeTime($book->date_create)); ?></p>
    <p>Дата редактирования: <?php echo Yii::$app->formatter->asDateTime($book->date_update); ?></p>
  </div>

